import { MantineProvider } from '@mantine/core'
import { ModalsProvider } from '@mantine/modals'
import { QueryClient, QueryClientProvider } from 'react-query'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'

import 'react-toastify/dist/ReactToastify.css'
import './App.css'
import RequireAuth from './components/RequireAuth'
import AuthProvider from './context/AuthProvider'
import Auth from './pages/Auth'
import Projects from './pages/Home'
import InterviewQuestion from './pages/InterviewQuestion'
import JobDescription from './pages/JobDesCreation'
import ListJobs from './pages/ListJobs'
import Redirect from './pages/Redirect'
import ScanCV from './pages/ScanFeatures/CV'
import ScanJob from './pages/ScanFeatures/Job'

const queryClient = new QueryClient()

function App() {
  return (
    <AuthProvider>
      <ModalsProvider>
        <QueryClientProvider client={queryClient}>
          <MantineProvider withGlobalStyles withNormalizeCSS>
            <ToastContainer />
            <BrowserRouter>
              <Routes>
                <Route
                  path="/"
                  element={
                    <RequireAuth>
                      <Projects />
                    </RequireAuth>
                  }
                />
                <Route path="/auth" element={<Auth />} />
                <Route path="/redirect" element={<Redirect />} />
                <Route
                  path="/generate-job-description"
                  element={
                    <RequireAuth>
                      <JobDescription />
                    </RequireAuth>
                  }
                />
                <Route
                  path="/list-job-description"
                  element={
                    <RequireAuth>
                      <ListJobs />
                    </RequireAuth>
                  }
                />
                <Route
                  path="/scan-cvs"
                  element={
                    <RequireAuth>
                      <ScanCV />
                    </RequireAuth>
                  }
                />
                <Route
                  path="/scan-jobs"
                  element={
                    <RequireAuth>
                      <ScanJob />
                    </RequireAuth>
                  }
                />
                <Route
                  path="/generate-interview-question"
                  element={
                    <RequireAuth>
                      <InterviewQuestion />
                    </RequireAuth>
                  }
                />
              </Routes>
            </BrowserRouter>
          </MantineProvider>
        </QueryClientProvider>
      </ModalsProvider>
    </AuthProvider>
  )
}

export default App
