import {
  AppShell,
  AppShellProps,
  Avatar,
  Container,
  Header,
  Image,
  Title,
  createStyles,
} from '@mantine/core'

import UserActions from '../UserActions'

const HEADER_HEIGHT = 64

const useStyles = createStyles((theme) => ({
  innerHeader: {
    height: HEADER_HEIGHT,
    background: '#971D62',
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: '100%',
    paddingLeft: theme.spacing.lg,
    paddingRight: theme.spacing.lg,
    maxWidth: '100%',
  },
  logo: {
    backgroundColor: '#ffffff',
  },
  boxHeader: {
    display: 'flex',
    alignItems: 'center',
    gap: theme.spacing.xs,
  },
  brandName: {
    color: '#ffffff',
  },
}))

export default function HeaderLayout() {
  const { classes, cx } = useStyles()
  return (
    <Header height={HEADER_HEIGHT} className={classes.innerHeader}>
      <Container className={classes.container}>
        <div className={classes.boxHeader}>
          <div>
            <Image
              className={classes.logo}
              mx="auto"
              radius="md"
              src="src/momo-logo.svg"
              alt="Logo MoMo"
            />
          </div>
          <div>
            <Title order={5} className={classes.brandName}>
              GenAI For Recruitment
            </Title>
          </div>
        </div>
        <UserActions />
      </Container>
    </Header>
  )
}
