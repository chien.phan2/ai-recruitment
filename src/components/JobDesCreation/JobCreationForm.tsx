import { FC } from 'react'

import {
  Button,
  Container,
  Paper,
  TextInput,
  Textarea
} from '@mantine/core'
import { useForm } from '@mantine/form'

const JobCreationForm: FC<{
  handleGenrate: (data: any) => void,
  loading?: boolean,
}> = ({ handleGenrate, loading }) => {
  const form = useForm({
    initialValues: {
      title: '',
      require: '',
    },

    validate: {
      title: (value: string) => (!value ? 'Title is required' : null),
      require: (value: string) => (!value ? 'Requirements is required' : null),
    },
  })

  return (
    <Container size={420} my={30}>
      <Paper withBorder shadow="md" p={30} radius="md">
        <form onSubmit={form.onSubmit((values) => handleGenrate(values))}>
          <TextInput
            disabled={loading}
            mb={16}
            label="Job Title"
            placeholder="Ex: Junior Mobile Developer"
            {...form.getInputProps('title')}
          />
          <Textarea
            disabled={loading}
            placeholder="Requirements for the job"
            label="Job Requirements"
            autosize
            minRows={2}
            {...form.getInputProps('require')}
          />
          <Button type="submit" fullWidth mt="xl" loading={loading}>
            Generate Job Description
          </Button>
        </form>
      </Paper>
    </Container>
  )
}
export default JobCreationForm
