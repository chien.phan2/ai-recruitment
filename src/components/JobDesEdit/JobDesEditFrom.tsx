import { FC } from 'react'

import { Button, Container, Paper, TextInput, Textarea } from '@mantine/core'
import { useForm } from '@mantine/hooks'

const JobEditForm: FC<{
  handleEdit: (data: any) => void
  loading?: boolean
  dataJob: {
    data: string
    jobTitle: string
  }
}> = ({ handleEdit, loading, dataJob }) => {
  const form = useForm({
    initialValues: {
      jobTitle: dataJob?.jobTitle || '',
      data: dataJob?.data || '',
    },
  })

  return (
    <Container size={800} my={30}>
      <Paper withBorder shadow="md" p={30} radius="md">
        <form onSubmit={form.onSubmit((values) => handleEdit(values))}>
          <TextInput
            mb={16}
            label="Job Title"
            placeholder="Ex: Junior Mobile Developer"
            {...form.getInputProps('jobTitle')}
          />
          <Textarea
            placeholder="Requirements for the job"
            label="Job Requirements"
            autosize
            minRows={2}
            {...form.getInputProps('data')}
          />
          <Button type="submit" fullWidth mt="xl" loading={loading}>
            Update
          </Button>
        </form>
      </Paper>
    </Container>
  )
}
export default JobEditForm
