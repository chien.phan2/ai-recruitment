/* eslint-disable react/no-danger */
import { FC } from 'react'

import { Container } from '@mantine/core'

const JobDesView: FC<{
  data: string
}> = ({ data }) => (
  <Container size='md'>
    {data?.split('\n')?.map((item: string) => (
      <p
        dangerouslySetInnerHTML={{
          __html: item,
        }}
      />
    ))}
  </Container>
)

export default JobDesView
