
import { AppShell, AppShellProps } from '@mantine/core'

import HeaderLayout from './Header'
import { NavbarNested } from './Nav'

export default function Layout(props: AppShellProps) {
  return <AppShell fixed header={<HeaderLayout />} {...props} navbar={<NavbarNested />} />
}
