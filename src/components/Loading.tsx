const MomoPageLoading = (props: any) => {
  let { width } = props
  const { coverStyleHeight } = props
  let coverStyle = {}
  if (!width) {
    width = '30'
  }

  if (coverStyleHeight) {
    coverStyle = {
      display: 'flex',
      justifyContent: 'center',
      height: `${coverStyleHeight}vh`,
    }
  } else {
    coverStyle = {
      display: 'flex',
      justifyContent: 'center',
      height: '20vh',
    }
  }

  const loadingSpinner = {
    width: `${width}px`,
    height: `${width * 0.9447852761}px`,
  }
  return (
    <div style={coverStyle}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          flexDirection: 'column',
        }}
      >
        <div className="loading-spinner" style={loadingSpinner}>
          <div className="icon-cover">
            <div />
            <div />
            <div />
            <div />
          </div>
        </div>
      </div>
    </div>
  )
}

export default MomoPageLoading
