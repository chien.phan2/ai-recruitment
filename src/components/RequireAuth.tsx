
import { Navigate, useLocation } from 'react-router-dom'

import useAuth from '../utils/useAuth'

function RequireAuth({ children }: { children: JSX.Element }) {
  const location = useLocation()
  const { user } = useAuth()

  if (!user) {
    return <Navigate to="/auth" state={{ from: location }} replace />
  }

  return children
}

export default RequireAuth
