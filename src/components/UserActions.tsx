import { FC } from 'react'

import { Avatar, Group, Text } from '@mantine/core'

import useAuth from '../utils/useAuth'

const UserActions: FC<any> = () => {
  const { user } = useAuth()

  if (!user?.email) return null

  return (
    <Group style={{ cursor: 'pointer' }}>
      <Avatar src={user?.avatar} alt={user?.name} radius="xl" />
      <Text ml={-10} size="sm" color="white">
        {user?.name}
      </Text>
    </Group>
  )
}

export default UserActions
