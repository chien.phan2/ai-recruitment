import { createContext, ProviderProps } from 'react'

import { useLocalStorage } from '@mantine/hooks'

interface AuthContextValue {
  user?: {
    email: string
    name: string
    avatar: string
  }
  setUser: (user: any) => void
}

const defaultAuthContextValue: AuthContextValue = {
  user: undefined,
  setUser: () => {},
}

export const AuthContext = createContext<AuthContextValue>(defaultAuthContextValue)

const AuthProvider = ({ children }: Omit<ProviderProps<AuthContextValue>, 'value'>) => {
  const [user, setUser] = useLocalStorage({ key: 'user', defaultValue: undefined })
  return <AuthContext.Provider value={{ user, setUser }}>{children}</AuthContext.Provider>
}

export default AuthProvider
