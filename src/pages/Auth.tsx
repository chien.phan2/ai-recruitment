import { FC } from 'react'

import { Button, Container, Image, createStyles } from '@mantine/core'
import { useNavigate } from 'react-router-dom'

import axiosClient from '../utils/axiosClient'
import { getCookie } from '../utils/getCookie'
import useAuth from '../utils/useAuth'
import useSession from '../utils/useSession'

const useStyles = createStyles((theme) => ({
  containerWrapper: {
    height: '50vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonWrapper: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: 48,
  },
  logo: {
    marginRight: theme.spacing.xs,
  },
}))

const Auth: FC<any> = () => {
  const { classes, cx } = useStyles()
  const { user, setUser } = useAuth()

  const navigate = useNavigate()

  const session = useSession()

  const cookieData = (getCookie('oauth2Code') || '').replace('%2F', '/')

  if (session && cookieData && user) {
    navigate('/', { replace: true })
  }

  const fetchAuthUser = async (cookie: any) => {
    const getAuth = await axiosClient.post('/auth/me', { code: cookie })
    const response = getAuth.data

    sessionStorage.setItem('token', JSON.stringify(response.token))
    setUser(response.user)
    navigate('/', { replace: true })
  }

  const handleLoginWithGG = async () => {

    let timer: any = null

    const googleLoginURL = 'http://localhost:9999/login/google'

    const newWindow = window.open(googleLoginURL, '_blank', 'width=500,height=600')

    if (newWindow) {
      timer = setInterval(() => {
        if (newWindow.closed) {
          const cookie = (getCookie('oauth2Code') || '').replace('%2F', '/')
          fetchAuthUser(cookie)
          if (timer) clearInterval(timer)
        }
      }, 500)
    }
  }

  return (
    <Container className={classes.containerWrapper}>
      {!session && (
        <Button
          variant="outline"
          className={classes.buttonWrapper}
          onClick={() => {
            handleLoginWithGG()
          }}
        >
          <div className={classes.logo}>
            <Image mx="auto" radius="md" src="src/logo-google.png" alt="Logo MoMo" />
          </div>
          Login with Google Account <span style={{ color: 'red' }}>(Only for MoMo team)</span>
        </Button>
      )}
    </Container>
  )
}

export default Auth
