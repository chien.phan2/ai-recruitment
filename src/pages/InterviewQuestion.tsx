/* eslint-disable no-irregular-whitespace */
/* eslint-disable react/jsx-curly-brace-presence */
/* eslint-disable object-shorthand */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-plusplus */
/* eslint-disable no-lonely-if */
/* eslint-disable no-nested-ternary */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useMemo, useState } from 'react'

import {
  Box,
  Button,
  Checkbox,
  CheckboxGroup,
  Container,
  Paper,
  Select,
  Text,
  createStyles,
} from '@mantine/core'
import { modals } from '@mantine/modals'
import { IconCircleCheckFilled, IconCircleXFilled, IconReload } from '@tabler/icons-react'
import { toast } from 'react-toastify'

import Layout from '../components/Layout'
import MomoPageLoading from '../components/Loading'
import { generateQuestion } from '../services/interviewQuesttion'
import { getListJob } from '../services/jobDescription'
import { useRequestProcessor } from '../utils/useRequestProcessor'
import useSession from '../utils/useSession'

const useStyles = createStyles((theme) => ({
  wrapperSelect: {
    // maxWidth: 400,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
  },

  wrapperQuestion: {
    border: '1px solid #e0e0e0',
    borderRadius: 16,
  },

  wrapperQuestionContainer: {
    padding: '16px 32px',
  },

  wrapperRadioRoot: {
    '.mantine-Group-root': {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
  },

  wrapperButton: {
    borderTop: '1px solid #e0e0e0',
    padding: '16px 32px',
    display: 'flex',
    justifyContent: 'flex-end',
  },

  buttonStyle: {
    WebkitTransition: 'all ease 0.4s',
    transition: 'all ease 0.4s',
    color: '#fff',
    backgroundColor: '#A50064',
    '&:hover': {
      backgroundColor: '#F45197',
    },
  },

  customAnswer: {
    display: 'flex',
    alignItems: 'center',
    '& > :nth-child(2)': {
      marginLeft: '8px',
    },
  },

  customCheckboxXColor: {
    color: '#FE0006',
  },

  customCheckboxColor: {
    color: '#0E9700',
  },

  wrapperTable: {
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
    marginTop: 48,
  },

  emptyClass: {
    marginTop: 48,
  },
}))

const InterviewQuestion: FC<{}> = () => {
  const { classes, cx } = useStyles()
  const token = useSession()
  const [dataJob, setDataJob] = useState<string | null>('')
  const [isDisable, setIsDisable] = useState<boolean>(false)
  const [correctUserAnswer, setCorrectUserAnswer] = useState<any>([])

  const { mutate } = useRequestProcessor()

  const getListJobs = mutate('ListJobs', getListJob, {
    onSuccess: () => {},
    onError: () => {
      toast.error('Get list JDs failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const dataInterviewQuestion = mutate('InterviewQuestion', generateQuestion, {
    onSuccess: () => {
      toast.success('Generate interview questions successfully!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
    onError: () => {
      toast.error('Generate interview questions failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  useEffect(() => {
    if (token) {
      getListJobs.mutate({ token } as any)
    }
  }, [token])

  const dataListJob = useMemo(() => {
    const data = getListJobs?.data?.data || []

    const dataSelect = data?.map((item: any) => ({
      value: item._id,
      label: item.jobTitle,
    }))

    return dataSelect
  }, [getListJobs?.data])

  const handleGenerateQuestion = () => {
    if (!dataJob) {
      toast.error('Please choose JD!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    } else {
      dataInterviewQuestion.mutate({ id: dataJob } as any)
    }
  }

  const [selectedCheckboxes, setSelectedCheckboxes] = useState<any>({})

  const checkUserAnswers = (correctAnswer: string[], answerOfUser: string[]) => {
    const totalQuestions = Object.keys(correctAnswer).length
    let correctCount = 0
    const correctQuestions = []

    for (const questionKey in correctAnswer) {
      if (
        Array.isArray(answerOfUser[questionKey]) &&
        JSON.stringify(correctAnswer[questionKey]) === JSON.stringify(answerOfUser[questionKey])
      ) {
        correctCount++
        correctQuestions.push(questionKey)
      }
    }

    const result = {
      totalCorrect: correctCount,
      totalQuestions: totalQuestions,
      correctQuestions: correctQuestions,
    }

    return result
  }

  const handleCheckboxChange = (event: any, questionIndex: any) => {
    const checkboxId = event
    const updatedSelectedCheckboxes = { ...selectedCheckboxes }

    if (event) {
      updatedSelectedCheckboxes[questionIndex] = [...checkboxId]
    } else {
      if (updatedSelectedCheckboxes[questionIndex]) {
        updatedSelectedCheckboxes[questionIndex] = updatedSelectedCheckboxes[questionIndex].filter(
          (value: any) => value !== checkboxId,
        )
      }
    }
    setSelectedCheckboxes(updatedSelectedCheckboxes)
  }

  const handleTransformAnswer = (answer: any, questionIndex: any) => {
    if (typeof Number(answer) === 'number' && !Number.isNaN(Number(answer))) {
      return answer + 1
    }

    if (answer?.length === 1) {
      const formatAnswer = answer.toLowerCase()
      let result = 0
      switch (formatAnswer) {
        case 'a':
          result = 1
          break

        case 'b':
          result = 2
          break

        case 'c':
          result = 3
          break

        case 'd':
          result = 4
          break

        default:
          result = 1
          break
      }

      return result
    }

    const dataQuestions = dataInterviewQuestion?.data?.data || []
    const question = dataQuestions[questionIndex]
    const correctAnswers = question?.correct_answers || []

    const indexOfAnswer = correctAnswers.indexOf(answer)

    return indexOfAnswer + 1
  }

  const correctAnswers = useMemo(() => {
    const data = dataInterviewQuestion?.data?.data || []
    const correctDataAnswers: any = {}

    data.forEach((item: any, questionIndex: number) => {
      const multipleAnswers = item?.correct_answers?.map(
        (answer: any) =>
          `question_${questionIndex + 1}_option_${handleTransformAnswer(answer, questionIndex)}`,
      )
      correctDataAnswers[questionIndex + 1] = multipleAnswers
    })

    return correctDataAnswers
  }, [dataInterviewQuestion?.data])

  const handleCheckAnswer = () => {
    setIsDisable(true)
    const result = checkUserAnswers(correctAnswers, selectedCheckboxes)
    setCorrectUserAnswer(result?.correctQuestions)
    modals.open({
      title: (
        <Text align="center" size={'lg'} weight={700}>
          Result
        </Text>
      ),
      children: (
        <Box>
          <Container size={'md'}>
            <Text size={'lg'} weight={400}>
              Your score:{' '}
              <strong>
                {result.totalCorrect}/{result.totalQuestions}
              </strong>
            </Text>
          </Container>
        </Box>
      ),
    })
  }

  return (
    <Layout>
      <Container size="lg">
        <div className={classes.wrapperSelect}>
          <div>
            <Select
              onChange={(value) => setDataJob(value)}
              label="Choose JD"
              placeholder="Pick one"
              searchable
              nothingFound="No options"
              data={dataListJob || []}
            />
          </div>
          <div>
            <Button
              variant="outline"
              onClick={() => handleGenerateQuestion()}
              radius="lg"
              size="lg"
              compact
            >
              <Text mr={12}>Generate</Text>
              <IconReload />
            </Button>
          </div>
        </div>

        {dataInterviewQuestion?.isLoading ? (
          <div
            className={dataInterviewQuestion?.isLoading ? classes.wrapperTable : classes.emptyClass}
          >
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '100px',
                marginBottom: '100px',
              }}
            >
              <MomoPageLoading width={100} />
            </div>
          </div>
        ) : dataInterviewQuestion?.data?.data ? (
          <Box mt={56} className={classes.wrapperQuestion}>
            <div className={classes.wrapperQuestionContainer}>
              {dataInterviewQuestion?.data?.data?.map((question: any, index: number) => (
                <Paper key={index} style={{ marginBottom: '1rem' }}>
                  <Text size="xl" weight={400} style={{ marginBottom: '0.5rem' }}>
                    <strong>Question {index + 1}:</strong> {question.question}
                  </Text>
                  <CheckboxGroup
                    id={`question_${index + 1}`}
                    className={classes.wrapperRadioRoot}
                    onChange={(value) => handleCheckboxChange(value, index + 1)}
                  >
                    {question.options.map((option: any, optionIndex: number) => (
                      <Checkbox
                        disabled={isDisable}
                        key={`question_${index + 1}_option_${optionIndex + 1}`}
                        id={`question_${index + 1}_option_${optionIndex + 1}`}
                        label={
                          <Box className={classes.customAnswer}>
                            <Text size="sm" weight={400}>
                              {option}
                            </Text>
                            {isDisable ? (
                              correctAnswers[index + 1]?.includes(
                                `question_${index + 1}_option_${optionIndex + 1}`,
                              ) ? (
                                correctUserAnswer.includes(`${index + 1}`) ? (
                                  <IconCircleCheckFilled className={classes.customCheckboxColor} />
                                ) : (
                                  <IconCircleXFilled className={classes.customCheckboxXColor} />
                                )
                              ) : null
                            ) : null}
                          </Box>
                        }
                        value={`question_${index + 1}_option_${optionIndex + 1}`}
                        checked={
                          selectedCheckboxes[index] &&
                          selectedCheckboxes[index].includes(
                            `question_${index + 1}_option_${optionIndex + 1}`,
                          )
                        }
                      />
                    ))}
                  </CheckboxGroup>
                </Paper>
              ))}
            </div>

            <div className={classes.wrapperButton}>
              <Button
                onClick={() => handleCheckAnswer()}
                radius="md"
                size="md"
                className={classes.buttonStyle}
                disabled={isDisable}
              >
                Submit
              </Button>
            </div>
          </Box>
        ) : null}
      </Container>
    </Layout>
  )
}

export default InterviewQuestion
