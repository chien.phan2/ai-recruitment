/* eslint-disable react/no-array-index-key */
/* eslint-disable react/no-danger */
/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useCallback, useMemo, useState } from 'react'

import { Container, Grid, Skeleton, createStyles } from '@mantine/core'
import { toast } from 'react-toastify'

import JobCreationForm from '../components/JobDesCreation/JobCreationForm'
import Layout from '../components/Layout'
import { genJobDes } from '../services/jobDescription'
import { useRequestProcessor } from '../utils/useRequestProcessor'
import useSession from '../utils/useSession'

const useStyles = createStyles((theme) => ({
  wrapper: {
    border: '1px solid #e3e3e3',
    padding: '16px 24px 16px 24px',
    borderRadius: '6px',
  },
}))

const child = <Skeleton height={140} radius="md" animate={false} />

const JobDescription: FC<{}> = () => {
  const { classes, cx } = useStyles()

  const token = useSession()

  const { mutate } = useRequestProcessor()

  const [keyword, setKeyword] = useState<string[]>([])

  const handleGenerateJD = useCallback(
    (data: { title: string; require: string }) => {
      const { title, require } = data

      const jobTitle = title
      const skills = require
        ?.split('\n')
        .map((item: string) => item.trim())
        .filter((item: string) => item !== '')

      setKeyword(skills)

      generateMutation.mutate({ jobTitle, skills, token } as any)
    },
    [token],
  )

  const generateMutation = mutate('generateJD', genJobDes, {
    onSuccess: () => {
      toast.success('Generate JD successfully!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
    onError: () => {
      toast.error('Generate JD failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const dataJob = useMemo(() => {
    let response = generateMutation?.data?.data || '';
    if (response) {
      keyword.forEach(item => {
        if (response?.toLowerCase().includes(item?.toLowerCase())) {
          response = response.replace(new RegExp(item, 'g'), `<b style="color: red;">${item}</b>`);
        }
      });
      return response
    }
    return response
  }, [generateMutation?.data?.data, keyword])

  return (
    <Layout>
      <Container size="lg">
        <Grid>
          <Grid.Col xs={generateMutation?.data ? 4 : 12}>
            <JobCreationForm
              handleGenrate={handleGenerateJD}
              loading={generateMutation?.isLoading}
            />
          </Grid.Col>
          {generateMutation?.data?.data && (
            <Grid.Col xs={8} pl={80} mt={30}>
              <div className={classes.wrapper}>
                {(dataJob || '')?.split('\n')?.map((item: string, index: number) => (
                  <p
                    key={index}
                    dangerouslySetInnerHTML={{
                      __html: item,
                    }}
                  />
                ))}
              </div>
            </Grid.Col>
          )}
        </Grid>
      </Container>
    </Layout>
  )
}

export default JobDescription
