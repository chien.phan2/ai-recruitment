/* eslint-disable no-underscore-dangle */
/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useState } from 'react'

import { Button, Container, ScrollArea, Switch, Table, createStyles } from '@mantine/core'
import { modals } from '@mantine/modals'
import { IconPencil, IconTrash } from '@tabler/icons-react'
import { toast } from 'react-toastify'

import JobEditForm from '../components/JobDesEdit/JobDesEditFrom'
import Layout from '../components/Layout'
import { getListJob, updateJobDetail } from '../services/jobDescription'
import { useRequestProcessor } from '../utils/useRequestProcessor'
import useSession from '../utils/useSession'

const useStyles = createStyles((theme) => ({
  header: {
    position: 'sticky',
    top: 0,
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    transition: 'box-shadow 150ms ease',
    zIndex: 11,

    '&::after': {
      content: '""',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 10,
      borderBottom: `1px solid ${
        theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[2]
      }`,
    },
  },

  scrolled: {
    boxShadow: theme.shadows.sm,
  },

  colStatus: {
    width: '10%',
  },

  colAction: {
    width: '15%',
  },

  centerCol: {
    display: 'flex',
    justifyContent: 'center',
  },

  flexAction: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
}))

const ListJobs: FC<{}> = () => {
  const { classes, cx } = useStyles()
  const [scrolled, setScrolled] = useState(false)
  const token = useSession()

  const { mutate, query, queryClient } = useRequestProcessor()

  const getListJobs = mutate('ListJobs', getListJob, {
    onSuccess: () => {},
    onError: () => {
      toast.error('Get list JDs failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const updateJob = mutate('updateJobDetail', updateJobDetail, {
    onSuccess: (data) => {
      if (data) {
        getListJobs.mutate({ token } as any)
        modals.closeAll();
        toast.success('Update JD successfully!', {
          position: 'top-right',
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: 'light',
        })
      }
    },
    onError: () => {
      toast.error('Update JD failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const handleChangeStatus = (status: boolean, id: string) => {
    updateJob.mutate({ status, id, token } as any)
  }

  const handleDeleteJob = (id: string) => {
    updateJob.mutate({ isDeleted: true, id, token } as any)
  }

  const getDataAfterEdit = (data: any, id: string) => {
    updateJob.mutate({ data: data?.data, jobTitle: data?.jobTitle, id, token } as any)
  }

  const handleEditJob = (job: any) => {
    modals.open?.({
      title: 'Edit Job Description',
      size: 700,
      children: (
        <JobEditForm
          dataJob={job}
          loading={updateJob?.isLoading}
          handleEdit={(data) => getDataAfterEdit(data, job?._id)}
        />
      ),
    })
  }

  useEffect(() => {
    if (token) {
      getListJobs.mutate({ token } as any)
    }
  }, [token])

  const data = getListJobs?.data || []

  const rows = (data?.data || []).map((item: any, index: number) => (
    <tr
      key={item._id}
      style={{
        textAlign: 'center',
      }}
    >
      <td className={classes.colStatus}>{index + 1}</td>
      <td>{item.jobTitle}</td>
      <td className={classes.colStatus}>
        <div className={classes.centerCol}>
          <Switch
            defaultChecked={item?.status}
            onChange={(e) => handleChangeStatus(e.currentTarget.checked, item._id)}
          />
        </div>
      </td>
      <td className={classes.colAction}>
        <div className={classes.flexAction}>
          <Button
            variant="outline"
            onClick={() => handleEditJob(item)}
            radius="lg"
            size="xs"
            compact
          >
            <IconPencil />
          </Button>
          <Button
            variant="outline"
            onClick={() => handleDeleteJob(item._id)}
            radius="lg"
            size="xs"
            compact
            color="red"
          >
            <IconTrash />
          </Button>
        </div>
      </td>
    </tr>
  ))

  const columns = [
    {
      title: 'STT',
      dataIndex: 'STT',
      key: 'id',
    },
    {
      title: 'Title',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
    },
  ]

  return (
    <Layout>
      <Container size="lg">
        <ScrollArea onScrollPositionChange={({ y }) => setScrolled(y !== 0)}>
          <Table mt={8} border={1}>
            <thead className={cx(classes.header, { [classes.scrolled]: scrolled })}>
              <tr>
                {columns.map((column) => (
                  <th key={`${column.key}`}>{column.title}</th>
                ))}
              </tr>
            </thead>
            <tbody>{rows}</tbody>
          </Table>
        </ScrollArea>
      </Container>
    </Layout>
  )
}

export default ListJobs
