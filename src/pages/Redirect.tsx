import { FC, useEffect } from 'react';

import { Container } from '@mantine/core';


const Redirect: FC<any> = () => {

  useEffect(() => {
    setTimeout(() => {
      window.close()
    }, 500);
  }, [])

  return (
    <Container>
      <p>Login success</p>
    </Container>
  )
}

export default Redirect