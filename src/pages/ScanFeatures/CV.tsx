/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react-hooks/exhaustive-deps */
import { FC, useEffect, useMemo, useState } from 'react'

import {
  Box,
  Button,
  Container,
  Grid,
  ScrollArea,
  Select,
  Table,
  Text,
  TextInput,
  createStyles,
} from '@mantine/core'
import { modals } from '@mantine/modals'
import { IconCircleCheckFilled, IconSearch } from '@tabler/icons-react'
import { toast } from 'react-toastify'

import Layout from '../../components/Layout'
import MomoPageLoading from '../../components/Loading'
import { getListJob } from '../../services/jobDescription'
import { findCVForJob, getListDrive } from '../../services/scanCV'
import { useRequestProcessor } from '../../utils/useRequestProcessor'
import useSession from '../../utils/useSession'

const useStyles = createStyles((theme) => ({
  wrapperSelect: {
    // maxWidth: 700,
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
  },

  wrapperTable: {
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
  },

  emptyClass: {},

  header: {
    position: 'sticky',
    top: 0,
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    transition: 'box-shadow 150ms ease',
    zIndex: 11,

    '&::after': {
      content: '""',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 10,
      borderBottom: `1px solid ${
        theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[2]
      }`,
    },
  },

  scrolled: {
    boxShadow: theme.shadows.sm,
  },

  colStatus: {
    width: '10%',
  },

  customCheckboxColor: {
    color: '#0E9700',
    marginLeft: 8,
  },
}))

const ScanCV: FC<{}> = () => {
  const { classes, cx } = useStyles()
  const [scrolled, setScrolled] = useState(false)
  const token = useSession()

  const [loading, setLoading] = useState(false)

  const [dataDrive, setDataDrive] = useState<string | null>('')
  const [dataJob, setDataJob] = useState<string | null>('')
  const [top, setTop] = useState<string>('25')

  const { mutate } = useRequestProcessor()

  const getListDrives = mutate('getListDrive', getListDrive, {
    onSuccess: () => {},
    onError: () => {
      toast.error('Get list drive failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const getListJobs = mutate('ListJobs', getListJob, {
    onSuccess: () => {},
    onError: () => {
      toast.error('Get list JDs failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const findCV = mutate('FindCV', findCVForJob, {
    onSuccess: () => {
      toast.success('Find CVs successfully!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
    onError: () => {
      toast.error('Find CVs failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  useEffect(() => {
    if (findCV?.data) {
      setLoading(false)
    }
  }, [findCV?.data])

  useEffect(() => {
    if (token) {
      getListDrives.mutate({ token } as any)
      getListJobs.mutate({ token } as any)
    }
  }, [token])

  const dataListDrive = useMemo(() => {
    const data = getListDrives?.data?.data || []

    const dataSelect = data?.map((item: any) => ({
      value: item.id,
      label: item.folderName,
    }))

    return dataSelect
  }, [getListDrives?.data])

  const dataListJob = useMemo(() => {
    const data = getListJobs?.data?.data || []

    const dataSelect = data?.map((item: any) => ({
      value: item._id,
      label: item.jobTitle,
    }))

    return dataSelect
  }, [getListJobs?.data])

  const renderFolderName = () => {
    const data = getListDrives?.data?.data?.find((item: any) => item.id === dataDrive)
    return <Text style={{
      display: 'flex',
      alignItems: 'center',
    }} weight={500}>{data?.folderName} <IconCircleCheckFilled className={classes.customCheckboxColor} /></Text>
  }

  const renderJob = () => {
    const data = getListJobs?.data?.data?.find((item: any) => item._id === dataJob)
    return <Text style={{
      display: 'flex',
      alignItems: 'center',
    }} weight={500}>{data?.jobTitle} <IconCircleCheckFilled className={classes.customCheckboxColor} /></Text>
  }

  const fakeLoading = () => {
    setLoading(true)
    modals.closeAll()
  }

  const handleScanCV = () => {
    const idDrive = dataDrive
    const idJob = dataJob

    if (!idDrive || !idJob) {
      toast.error('Please choose Drive folder and JD!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    }

    findCV.mutate({ folderId: idDrive, jdId: idJob, top, token } as any)

    modals.open({
      title: <Text weight={700}>Start to get CVs</Text>,
      size: 500,
      centered: true,
      children: (
        <Container>
          <Grid>
            <Grid.Col xs={6}>
              <Box mb={8}>
                <Text>- Folder Drive to scan:</Text>
              </Box>
            </Grid.Col>
            <Grid.Col xs={6}>
              <Box mb={8}>{renderFolderName()}</Box>
            </Grid.Col>
            <Grid.Col xs={6}>
              <Box mb={8}>
                <Text>- Job Description to scan:</Text>
              </Box>
            </Grid.Col>
            <Grid.Col xs={6}>
              <Box mb={8}>{renderJob()}</Box>
            </Grid.Col>
            <Grid.Col
              xs={6}
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <Box>
                <Text>- Number of CVs:</Text>
              </Box>
            </Grid.Col>
            <Grid.Col xs={6}>
              <Box>
                <TextInput
                  type="number"
                  mb={16}
                  // label="Number of CVs"
                  placeholder="Ex: 10"
                  onChange={(e) => setTop(e.currentTarget.value)}
                  style={{
                    marginBottom: 0,
                  }}
                />
              </Box>
            </Grid.Col>
            <Grid.Col xs={12}>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <Button mt="xl" loading={loading} onClick={() => fakeLoading()}>
                  Start
                </Button>
              </div>
            </Grid.Col>
          </Grid>
        </Container>
      ),
    })
  }

  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'id',
    },
    {
      title: 'Link CV',
      dataIndex: 'url',
      key: 'url',
    },
    {
      title: 'Matching Rate',
      dataIndex: 'percent',
      key: 'percent',
    },
  ]

  const rows = (findCV?.data?.data || [])?.slice(0, Number(top)).map((item: any, index: number) => (
    <tr key={index}>
      <td className={classes.colStatus}>{index + 1}</td>
      <td>
        <a href={item?.url || ''} target="_blank" rel="noopener noreferrer">
          {item?.url || ''}
        </a>
      </td>
      <td>{item?.percent || 0}%</td>
    </tr>
  ))

  return (
    <Layout>
      <Container size="lg" mt={30}>
        <div className={classes.wrapperSelect}>
          <div>
            <Select
              onChange={(value) => setDataDrive(value)}
              label="Choose Drive Folder"
              placeholder="Pick one"
              searchable
              nothingFound="No options"
              data={dataListDrive || []}
            />
          </div>
          <div>
            <Select
              onChange={(value) => setDataJob(value)}
              label="Choose JD"
              placeholder="Pick one"
              searchable
              nothingFound="No options"
              data={dataListJob || []}
            />
          </div>
          <div>
            <Button variant="outline" onClick={() => handleScanCV()} radius="lg" size="lg" compact>
              <Text mr={12}>Find CVs</Text>
              <IconSearch />
            </Button>
          </div>
        </div>
        <ScrollArea
          mt={48}
          className={findCV?.isLoading ? classes.wrapperTable : classes.emptyClass}
          onScrollPositionChange={({ y }) => setScrolled(y !== 0)}
        >
          {findCV?.isLoading && loading ? (
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '100px',
                marginBottom: '100px',
              }}
            >
              <MomoPageLoading width={100} />
            </div>
          ) : findCV?.data ? (
            <Table mt={36} border={1}>
              <thead className={cx(classes.header, { [classes.scrolled]: scrolled })}>
                <tr>
                  {columns.map((column) => (
                    <th key={`${column.key}`}>{column.title}</th>
                  ))}
                </tr>
              </thead>
              <tbody>{rows}</tbody>
            </Table>
          ) : null}
        </ScrollArea>
      </Container>
    </Layout>
  )
}
export default ScanCV
