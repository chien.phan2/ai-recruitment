/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-nested-ternary */
import { FC, useState } from 'react'

import {
  Button,
  Container,
  Input,
  ScrollArea,
  Table,
  Text,
  TextInput,
  createStyles,
} from '@mantine/core'
import { modals } from '@mantine/modals'
import { IconEye, IconSearch, IconUpload } from '@tabler/icons-react'
import { toast } from 'react-toastify'

import JobDesView from '../../components/JobDesView/JobDesView'
import Layout from '../../components/Layout'
import MomoPageLoading from '../../components/Loading'
import { getListJob } from '../../services/jobDescription'
import { useRequestProcessor } from '../../utils/useRequestProcessor'
import useSession from '../../utils/useSession'

const useStyles = createStyles((theme) => ({
  wrapperInput: {
    // maxWidth: 700,
    display: 'flex',
    width: '100%',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
  },

  header: {
    position: 'sticky',
    top: 0,
    backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.white,
    transition: 'box-shadow 150ms ease',
    zIndex: 11,

    '&::after': {
      content: '""',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 0,
      zIndex: 10,
      borderBottom: `1px solid ${
        theme.colorScheme === 'dark' ? theme.colors.dark[3] : theme.colors.gray[2]
      }`,
    },
  },

  scrolled: {
    boxShadow: theme.shadows.sm,
  },

  colStatus: {
    width: '10%',
  },

  customLabel: {
    display: 'inline-block',
    marginBottom: 8,
    fontSize: 14,
    fontWeight: 500,
    color: '#212529',
    wordBreak: 'break-word',
    cursor: 'default',
  },

  wrapperFile: {
    // position: 'relative',
    height: 32,
    opacity: 0,
  },

  customInputFile: {
    position: 'absolute',
    top: -3,
    height: 36,
    border: '1px solid #ced4da',
    borderRadius: 4,
    width: 345,
    zIndex: -1,
    display: 'flex',
    alignItems: 'center',
  },

  wrapperTable: {
    border: '1px solid #e3e3e3',
    padding: '16px',
    borderRadius: '6px',
  },

  emptyClass: {},
}))

const ScanJob: FC<{}> = () => {
  const { classes, cx } = useStyles()
  const [file, setFile] = useState<FileList | null>(null)
  const [scrolled, setScrolled] = useState(false)
  const [loading, setLoading] = useState(false)
  const [top, setTop] = useState<string>('1')

  const [listJob, setListJob] = useState<any[]>([])

  const token = useSession()

  const handleScanJob = async () => {
    if (!file) {
      toast.error('Please upload your resume!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
      return
    }

    setLoading(true)

    const fileUpload: any = file?.[0]
    const form = new FormData()

    form.append('file', fileUpload, fileUpload?.name)
    form.append('token', JSON.stringify(token))
    form.append('top', top)

    const requestOptions: any = {
      method: 'POST',
      body: form,
      redirect: 'follow',
    }

    try {
      const response = await fetch('http://localhost:9999/find-jobs-for-cv', requestOptions)
      const result = await response.json()
      setListJob(result)
      setLoading(false)
      toast.success('Find Jobs successfully!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    } catch (error) {
      setLoading(false)
      console.log('error', error)
    }
  }

  const { mutate } = useRequestProcessor()

  const getListJobs = mutate('ListJobs', getListJob, {
    onSuccess: () => {},
    onError: () => {
      toast.error('Get list JDs failed!', {
        position: 'top-right',
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: 'light',
      })
    },
  })

  const columns = [
    {
      title: 'STT',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Job Description',
      dataIndex: 'jobTitle',
      key: 'jobTitle',
    },
    {
      title: 'Matching Rate',
      dataIndex: 'percent',
      key: 'percent',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
    },
  ]

  const handleViewJob = (id: string) => {
    getListJobs.mutate({ token, id } as any)

    if (getListJobs?.data) {
      modals.open({
        title: 'View Job',
        size: 800,
        centered: true,
        children: <JobDesView data={getListJobs?.data?.data?.[0]?.data} />,
      })
    }
  }

  const rows = (listJob || []).map((item: any, index: number) => (
    <tr key={item.id}>
      <td className={classes.colStatus}>{index + 1}</td>
      <td>{item.jobTitle}</td>
      <td>{item.percent}%</td>
      <td>
        <Button
          variant="outline"
          onClick={() => {
            handleViewJob(item.id)
          }}
          radius="lg"
          size="xs"
          compact
        >
          <IconEye />
        </Button>
      </td>
    </tr>
  ))

  return (
    <Layout>
      <Container size="lg" mt={30}>
        <div className={classes.wrapperInput}>
          <div>
            <label className={classes.customLabel}>Upload your CV</label>
            <div
              style={{
                position: 'relative',
              }}
            >
              <div className={classes.wrapperFile}>
                <Input
                  type="file"
                  // placeholder="Your resume"
                  onChange={(e) => setFile(e.target.files)}
                  icon={<IconUpload size={14} />}
                  size="sm"
                />
              </div>
              <div className={classes.customInputFile}>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    padding: '0 4px',
                    width: 40,
                    height: 34,
                    borderRight: '1px solid #ced4da',
                  }}
                >
                  <IconUpload />
                </div>
                <div
                  style={{
                    marginLeft: 16,
                  }}
                >
                  <Text size="sm">{file?.[0]?.name || 'Choose your CV to join with us!'}</Text>
                </div>
              </div>
            </div>
          </div>
          <div>
            <TextInput
              type="number"
              label="Number of Jobs"
              placeholder="Ex: 10"
              onChange={(e) => setTop(e.currentTarget.value)}
              style={{
                marginBottom: 0,
              }}
            />
          </div>
          <div>
            <Button variant="outline" onClick={() => handleScanJob()} radius="lg" size="lg" compact>
              <Text mr={12}>Find Jobs</Text>
              <IconSearch />
            </Button>
          </div>
        </div>

        <ScrollArea
          mt={48}
          className={loading ? classes.wrapperTable : classes.emptyClass}
          onScrollPositionChange={({ y }) => setScrolled(y !== 0)}
        >
          {loading ? (
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: '100px',
                marginBottom: '100px',
              }}
            >
              <MomoPageLoading width={100} />
            </div>
          ) : listJob?.length > 0 ? (
            <Table mt={36} border={1}>
              <thead className={cx(classes.header, { [classes.scrolled]: scrolled })}>
                <tr>
                  {columns.map((column) => (
                    <th key={`${column.key}`}>{column.title}</th>
                  ))}
                </tr>
              </thead>
              <tbody>{rows}</tbody>
            </Table>
          ) : null}
        </ScrollArea>
      </Container>
    </Layout>
  )
}

export default ScanJob
