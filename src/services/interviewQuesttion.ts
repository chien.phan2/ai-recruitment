import axiosClient from "../utils/axiosClient";

const generateQuestion = async (payload: any) => {
  try {
    const response = await axiosClient.get(`/generate-interview-questions?jd=${payload?.id}`);

    return response.data;
  } catch (error) {
    console.error("Failed to get list jobs: ", error);
    throw error;
  }
}

export {
  generateQuestion
};
