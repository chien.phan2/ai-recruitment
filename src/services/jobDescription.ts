/* eslint-disable react-hooks/rules-of-hooks */
import axiosClient from "../utils/axiosClient";

const genJobDes = async (payload: any) => {
  try {
    const response = await axiosClient.post("/generate/job-description", {
      ...payload,
    });

    return response.data;
  } catch (error) {
    console.error("Failed to generate job description: ", error);
    throw error;
  }
}

const getListJob = async (payload: any) => {
  try {
    const response = await axiosClient.post("/list/job-descriptions", {
      ...payload,
    });

    return response.data;
  } catch (error) {
    console.error("Failed to get list jobs: ", error);
    throw error;
  }
}

const updateJobDetail = async (payload: any) => {
  try {
    const response = await axiosClient.post("/update/job-description", {
      ...payload,
    });

    return response.data;
  } catch (error) {
    console.error("Failed to get list jobs: ", error);
    throw error;
  }
}

export {
  genJobDes,
  getListJob,
  updateJobDetail
};
