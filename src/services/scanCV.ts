import axiosClient from "../utils/axiosClient";

const getListDrive = async (payload: any) => {
  try {
    const response = await axiosClient.post("/list/drive-folders", {
      ...payload,
    });

    return response.data;
  } catch (error) {
    console.error("Failed to get list drive: ", error);
    throw error;
  }
}

const findCVForJob = async (payload: any) => {
  try {
    const response = await axiosClient.post("/find-cvs-for-job", {
      ...payload,
    });

    return response.data;
  } catch (error) {
    console.error("Failed to find CV for this JD: ", error);
    throw error;
  }
}

export {
  findCVForJob, getListDrive
};

