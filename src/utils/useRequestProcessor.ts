/* eslint-disable react-hooks/rules-of-hooks */
import {
  MutationFunction,
  QueryFunction,
  QueryOptions,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";
import { MutationOptions, QueryClient } from "react-query/core";

type QueryKey = string | readonly unknown[];

export function useRequestProcessor() {
  const queryClient: QueryClient = useQueryClient();

  function query<TData, TError = unknown>(
    key: QueryKey,
    queryFunction: QueryFunction<TData>,
    options: QueryOptions<TData, TError, any> = {},
    enabled?: boolean
  ) {
    return useQuery<TData, TError>({
      queryKey: key,
      queryFn: queryFunction,
      enabled,
      ...options,
    });
  }

  function mutate<TData>(
    key: QueryKey,
    mutationFunction: MutationFunction<TData>,
    options: MutationOptions<TData> = {}
  ) {
    return useMutation<TData>({
      mutationKey: key,
      mutationFn: mutationFunction,
      onSettled: () => queryClient.invalidateQueries(key),
      ...options,
    });
  }

  return { query, mutate, queryClient };
}
