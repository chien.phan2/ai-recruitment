/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from "react"

import { useNavigate } from "react-router-dom"

import { eraseCookie } from "./getCookie"
import useAuth from "./useAuth"

const useSession = () => {
  const { setUser } = useAuth()
  const [session, setSession] = useState(null)
  const navigate = useNavigate()

  useEffect(() => {
    const sessionData = sessionStorage.getItem('token') ? JSON.parse(sessionStorage.getItem('token') || '') : null
    if (sessionData) {
      setSession(sessionData)
    } else {
      setSession(null)
      setUser({})
      eraseCookie("oauth2Code")
      navigate('/auth', { replace: true })
    }
  }, [])
  return session
}

export default useSession